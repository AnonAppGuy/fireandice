# A client to fetch information over anapioficeandfire api based on clean architecture principals

Uses android architecture components.

> The project was run on Android Studio Arctic fox.
> Latest build tools requires java 11 runtime installed and may throw an error otherwise. See **Troubleshooting** for resolution.

#### Coded in kotlin and uses android architecture components like

- Uses MVVM architecture with data binding
- LiveData and Stateflow for demonstration of observable data sources,
- ViewModel for Activity/Fragment lifecycle awareness
- Kotlin CoroutinesApi for async tasks
- Dagger-hilt for dependency injection
- Error handling
- Network connectivity handling
- Pagination handling (non jetpack compose implementation)
- Unit, integration and UI tests
- CI/CD integration in `.travis.yaml` file

#### Instructions:

- to run app `./gradlew installDebug`
- to run all unit tests run SuiteUnitTest or in terminal `./gradlew test`
- to run all integration tests run SuiteIntegratedTest or in terminal `./gradlew connectedAndroidTest`

#### For unit testing, integration and UI testing.

- SuiteUnitTest for all unit tests
- SuiteIntegratedTest for all integration/UI tests
- Uses Junit, Mockito, Espresso, Google Truth

#### Troubleshooting:

```
* What went wrong:
Execution failed for task ':app:kaptDebugKotlin'.
> A failure occurred while executing org.jetbrains.kotlin.gradle.internal.KaptExecution
   > java.lang.reflect.InvocationTargetException (no error message)

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.
```

To resolve

1. make sure you have java 11 and added to java home.
2. Or On arctic fox go to Android Studio>Preferences>Build, Execution & Deployment>Build Tools>Gradle>Project>Gradle JDK>11, then Run from Android Studio
   ![picture 1](./images/d7384293bb20bcb855ccb333ea8c3bf8c6c299e78f95a20dc0e7d1d9d085c772.png)

#### Screenshots

![picture 2](./images/1634240566.png)
![picture 3](./images/1634240760.png)
![picture 4](./images/1634240826.png)
