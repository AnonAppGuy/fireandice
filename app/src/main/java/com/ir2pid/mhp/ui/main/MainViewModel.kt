package com.ir2pid.mhp.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ir2pid.mhp.data.Event
import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.data.repository.main.MainRepository
import com.ir2pid.mhp.util.DispatcherProvider
import com.ir2pid.mhp.util.NetworkObserver
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository,
    private val dispatchers: DispatcherProvider,
    val networkObserver: NetworkObserver
) : ViewModel() {

    private var lastRequestedPage = 1
    private var isRequestRunning = false
    private val visibleThreshold = 5

    private val _data = MutableLiveData<Event>(Event.Empty)
    val data: LiveData<Event> = _data
    private var _houseList = mutableListOf<GotResponse>()

    init {
        getHouseList(lastRequestedPage)
    }

    private fun getHouseList(page: Int) {
        if (isRequestRunning) return
        isRequestRunning = true
        viewModelScope.launch(dispatchers.io) {
            _data.postValue(Event.Loading)
            when (val response = repository.getAllHouses(page)) {
                is Resource.Error -> response.message?.let {
                    _data.postValue(Event.Failure(response.message))
                }
                is Resource.Success -> response.data?.let {
                    _houseList.addAll(it)
                    _data.postValue(Event.Success(_houseList))
                }
            }
            isRequestRunning = false
        }
    }

    fun listScrolled(visibleItemCount: Int, lastVisibleItemPosition: Int, totalItemCount: Int) {
        if (visibleItemCount + lastVisibleItemPosition + visibleThreshold > totalItemCount && !isRequestRunning) {
            getHouseList(++lastRequestedPage)
        }
    }
}