package com.ir2pid.mhp.ui.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ir2pid.mhp.data.Event
import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.repository.detail.DetailRepository
import com.ir2pid.mhp.util.DispatcherProvider
import com.ir2pid.mhp.util.NetworkObserver
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class DetailViewModel @ViewModelInject constructor(
    private val repository: DetailRepository,
    private val dispatchers: DispatcherProvider,
    val networkObserver: NetworkObserver
) : ViewModel() {

    private var _houseUrl = ""
    private val _data = MutableStateFlow<Event>(Event.Empty)
    val data: StateFlow<Event> = _data

    fun getHouse(houseUrl: String) {
        if (data.value !is Event.Success<*>) {
            _houseUrl = houseUrl
            getData(_houseUrl)
        }
    }

    private fun getData(houseUrl: String) {
        viewModelScope.launch(dispatchers.io) {
            _data.value = Event.Loading
            when (val response = repository.getHouse(houseUrl)) {
                is Resource.Error -> response.message?.let { _data.value = (Event.Failure(it)) }
                is Resource.Success -> response.data?.let { _data.value = (Event.Success(it)) }
            }
        }
    }
}