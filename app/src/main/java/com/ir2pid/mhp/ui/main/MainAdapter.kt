package com.ir2pid.mhp.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ir2pid.mhp.R
import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.databinding.HolderDataBinding
import com.ir2pid.mhp.util.OnItemClick

class MainAdapter(private val onClick: OnItemClick) :
    ListAdapter<GotResponse, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holderMainBinding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), R.layout.holder_data, parent, false
        )
        return MainViewHolder(holderMainBinding)
    }

    override fun getItemCount(): Int {
        return if (currentList.isNullOrEmpty()) 0 else currentList.size
    }

    override fun getItem(position: Int): GotResponse {
        return currentList[position]
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MainViewHolder).onBind(getItem(position))
    }

    inner class MainViewHolder(private val viewDataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {
        fun onBind(data: GotResponse) {
            (viewDataBinding as HolderDataBinding).data = data
            viewDataBinding.onClick = onClick
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<GotResponse>() {
            override fun areItemsTheSame(oldItem: GotResponse, newItem: GotResponse): Boolean =
                oldItem.url == newItem.url

            override fun areContentsTheSame(oldItem: GotResponse, newItem: GotResponse): Boolean =
                oldItem == newItem
        }
    }
}