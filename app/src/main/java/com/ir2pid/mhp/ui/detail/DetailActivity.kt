package com.ir2pid.mhp.ui.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.ir2pid.mhp.R
import com.ir2pid.mhp.data.Event
import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.databinding.ActivityDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.*

@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private val viewModel: DetailViewModel by viewModels()
    private var houseUrl = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        intent.apply {
            val key = getString(R.string.data)
            getStringExtra(key)?.let { houseUrl = it }
            setupObservables()
        }
    }

    private fun setupObservables() {
        viewModel.networkObserver.observe(this@DetailActivity, {
            if (it) {
                binding.network.networkCard.visibility = View.GONE
                viewModel.getHouse(houseUrl)
            } else {
                binding.network.networkCard.visibility = View.VISIBLE
            }
        })

        lifecycleScope.launchWhenStarted {
            viewModel.data.collect { event: Event ->
                when (event) {
                    is Event.Success<*> -> {
                        binding.loading.loadingCard.visibility = View.GONE
                        binding.data = event.data as GotResponse?
                    }
                    is Event.Failure -> {
                        binding.loading.loadingCard.visibility = View.VISIBLE
                        Toast.makeText(this@DetailActivity, event.errorText, Toast.LENGTH_SHORT)
                            .show()
                    }
                    is Event.Loading -> {
                        binding.loading.loadingCard.visibility = View.VISIBLE
                    }
                    else -> Unit
                }
            }
        }
    }
}