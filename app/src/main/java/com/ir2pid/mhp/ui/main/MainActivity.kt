package com.ir2pid.mhp.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ir2pid.mhp.R
import com.ir2pid.mhp.data.Event
import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.databinding.ActivityMainBinding
import com.ir2pid.mhp.ui.detail.DetailActivity
import com.ir2pid.mhp.util.OnItemClick
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    private val mAdapter by lazy {
        MainAdapter(object : OnItemClick {
            override fun click(data: GotResponse) {
                startActivity(Intent(this@MainActivity, DetailActivity::class.java).apply {
                    putExtra(getString(R.string.data), data.url)
                })
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.recycleView.adapter = mAdapter
        setupScrollListener()
        setupObservables()
    }

    private fun setupObservables() {
        viewModel.networkObserver.observe(this@MainActivity, {
            if (it) {
                binding.network.networkCard.visibility = View.GONE
            } else {
                binding.network.networkCard.visibility = View.VISIBLE
            }
        })

        viewModel.data.observe(this@MainActivity, { event ->
            when (event) {
                is Event.Success<*> -> {
                    binding.loading.loadingCard.visibility = View.GONE
                    mAdapter.submitList((event.data as MutableList<GotResponse>).toList())
                }
                is Event.Failure -> {
                    binding.loading.loadingCard.visibility = View.VISIBLE
                    Toast.makeText(this@MainActivity, event.errorText, Toast.LENGTH_SHORT)
                        .show()
                }
                is Event.Loading -> {
                    binding.loading.loadingCard.visibility = View.VISIBLE
                }
                else -> Unit
            }
        })
    }

    private fun setupScrollListener() {
        val layoutManager = binding.recycleView.layoutManager as LinearLayoutManager
        binding.recycleView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }
}