package com.ir2pid.mhp.di

import com.ir2pid.mhp.BuildConfig
import com.ir2pid.mhp.data.repository.detail.DetailApi
import com.ir2pid.mhp.data.repository.detail.DetailRepository
import com.ir2pid.mhp.data.repository.detail.DetailRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module
@InstallIn(ActivityRetainedComponent::class)
class DetailModule {

    @ActivityRetainedScoped
    @Provides
    fun provideDetailRepository(api: DetailApi): DetailRepository =
        DetailRepositoryImpl(api)

    @ActivityRetainedScoped
    @Provides
    fun provideDetailApi(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): DetailApi = Retrofit.Builder()
        .baseUrl(BuildConfig.HTTP_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(DetailApi::class.java)

}