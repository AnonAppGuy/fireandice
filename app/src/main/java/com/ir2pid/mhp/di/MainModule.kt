package com.ir2pid.mhp.di

import com.ir2pid.mhp.BuildConfig
import com.ir2pid.mhp.data.repository.main.MainApi
import com.ir2pid.mhp.data.repository.main.MainRepository
import com.ir2pid.mhp.data.repository.main.MainRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(ActivityRetainedComponent::class)
class MainModule {

    @ActivityRetainedScoped
    @Provides
    fun provideMainRepository(api: MainApi): MainRepository =
        MainRepositoryImpl(api)

    @ActivityRetainedScoped
    @Provides
    fun provideMainApi(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): MainApi = Retrofit.Builder()
        .baseUrl(BuildConfig.HTTP_BASE_URL)
        .addConverterFactory(gsonConverterFactory)
        .client(okHttpClient)
        .build()
        .create(MainApi::class.java)
}
