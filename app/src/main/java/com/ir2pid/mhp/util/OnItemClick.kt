package com.ir2pid.mhp.util

import com.ir2pid.mhp.data.models.GotResponse

interface OnItemClick {
    fun click(data: GotResponse)
}
