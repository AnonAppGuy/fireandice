package com.ir2pid.mhp.data.repository.detail

import com.ir2pid.mhp.data.models.GotResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface DetailApi {
    @GET
    suspend fun getHouseDetail(@Url url: String): Response<GotResponse>
}