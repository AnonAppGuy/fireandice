package com.ir2pid.mhp.data

sealed class Event {
    class Success<T>(val data: T) : Event()
    class Failure(val errorText: String) : Event()
    object Loading : Event()
    object Empty : Event()
}