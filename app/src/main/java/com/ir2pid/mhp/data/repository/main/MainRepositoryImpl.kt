package com.ir2pid.mhp.data.repository.main

import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val api: MainApi
) : MainRepository {
    override suspend fun getAllHouses(page: Int): Resource<List<GotResponse>> {
        return try {
            val response = api.getAllHouses(page)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occurred")
        }
    }
}