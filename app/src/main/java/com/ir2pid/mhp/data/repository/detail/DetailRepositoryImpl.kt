package com.ir2pid.mhp.data.repository.detail

import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse
import javax.inject.Inject

class DetailRepositoryImpl @Inject constructor(
    private val api: DetailApi
) : DetailRepository {
    override suspend fun getHouse(houseUrl: String): Resource<GotResponse> {
        return try {
            val response = api.getHouseDetail(houseUrl)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occurred")
        }
    }
}