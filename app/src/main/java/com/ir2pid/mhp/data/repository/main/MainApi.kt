package com.ir2pid.mhp.data.repository.main

import com.ir2pid.mhp.data.models.GotResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MainApi {
    @GET("api/houses")
    suspend fun getAllHouses(
        @Query(value = "page") page: Int,
        @Query(value = "pageSize") pageSize: Int = 10,
    ): Response<List<GotResponse>>
}