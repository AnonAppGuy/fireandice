package com.ir2pid.mhp.data.repository.detail

import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse

interface DetailRepository {
    suspend fun getHouse(houseUrl: String): Resource<GotResponse>
}