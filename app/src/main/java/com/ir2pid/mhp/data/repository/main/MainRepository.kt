package com.ir2pid.mhp.data.repository.main

import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse

interface MainRepository {
    suspend fun getAllHouses(page: Int): Resource<List<GotResponse>>
}