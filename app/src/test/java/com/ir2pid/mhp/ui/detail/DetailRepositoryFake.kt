package com.ir2pid.mhp.ui.detail

import com.ir2pid.mhp.data.DetailApiFake
import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.data.repository.detail.DetailRepository

open class DetailRepositoryFake(private val api: DetailApiFake) : DetailRepository {
    override suspend fun getHouse(houseUrl: String): Resource<GotResponse> {
        val response = api.getHouseDetail("")
        return Resource.Success(response.body()!!)
    }
}
