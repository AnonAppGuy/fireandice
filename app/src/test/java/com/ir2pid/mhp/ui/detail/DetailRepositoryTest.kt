package com.ir2pid.mhp.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ir2pid.mhp.MainCoroutineRule
import com.ir2pid.mhp.data.DetailApiFake
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailRepositoryTest {

    private lateinit var detailRepositoryFake: DetailRepositoryFake

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setup() {
        detailRepositoryFake = DetailRepositoryFake(DetailApiFake())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `repository getHouse returns house with name "House Name"`() {
        mainCoroutineRule.runBlockingTest {
            val name = "House Name"
            val response = detailRepositoryFake.getHouse("")
            assertThat(response.data?.name).isEqualTo(name)
        }
    }
}