package com.ir2pid.mhp.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ir2pid.mhp.MainCoroutineRule
import com.ir2pid.mhp.data.Resource
import com.ir2pid.mhp.data.models.GotResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class DetailViewModelTest {

    private lateinit var detailRepositoryFake: DetailRepositoryFake
    private lateinit var response: Resource.Success<GotResponse>

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setup() {
        detailRepositoryFake = mock(DetailRepositoryFake::class.java)
        val mock = GotResponse(
            "",
            "House Name",
            "Region",
            "Coat of arms",
            "",
            listOf(),
            listOf(),
            "",
            "",
            "",
            "",
            "",
            "",
            listOf(),
            listOf(),
            listOf()
        )
        response = Resource.Success(mock)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getHouse returns house detail`() {
        mainCoroutineRule.runBlockingTest {
            `when`(detailRepositoryFake.getHouse("")).thenReturn(response)
            assertThat(detailRepositoryFake.getHouse("")).isNotNull()
        }
    }
}