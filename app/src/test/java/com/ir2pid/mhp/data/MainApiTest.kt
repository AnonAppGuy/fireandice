package com.ir2pid.mhp.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ir2pid.mhp.MainCoroutineRule
import com.ir2pid.mhp.data.repository.main.MainApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MainApiTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var mainApi: MainApi

    @Before
    fun setup() {
        mainApi = MainApiFake()
    }

    @Test
    fun `MainApi getAllHouses returns house list`() {
        mainCoroutineRule.runBlockingTest {
            val result = mainApi.getAllHouses(0)
            assertThat(result).isNotNull()
        }
    }

    @Test
    fun `MainApi getAllHouses returns 3 houses`() {
        mainCoroutineRule.runBlockingTest {
            val result = mainApi.getAllHouses(0)
            assertThat(result.body()?.size).isEqualTo(3)
        }
    }
}