package com.ir2pid.mhp.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.ir2pid.mhp.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class DetailApiTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var detailApiFake: DetailApiFake

    @Before
    fun setup() {
        detailApiFake = DetailApiFake()
    }

    @Test
    fun `MainApi getHouseDetail returns valid data`() {
        mainCoroutineRule.runBlockingTest {
            val result = detailApiFake.getHouseDetail("")
            assertThat(result).isNotNull()
        }
    }
}