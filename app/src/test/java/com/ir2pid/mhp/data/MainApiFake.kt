package com.ir2pid.mhp.data

import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.data.repository.main.MainApi
import retrofit2.Response


class MainApiFake : MainApi {
    override suspend fun getAllHouses(page: Int, pageSize: Int): Response<List<GotResponse>> {
        val mock = GotResponse(
            "",
            "House Name",
            "Region",
            "Coat of arms",
            "",
            listOf(),
            listOf(),
            "",
            "",
            "",
            "",
            "",
            "",
            listOf(),
            listOf(),
            listOf()
        )
        return Response.success(listOf(mock, mock, mock))
    }
}