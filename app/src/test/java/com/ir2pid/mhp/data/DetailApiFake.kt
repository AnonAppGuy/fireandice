package com.ir2pid.mhp.data

import com.ir2pid.mhp.data.models.GotResponse
import com.ir2pid.mhp.data.repository.detail.DetailApi
import retrofit2.Response


class DetailApiFake : DetailApi {
    override suspend fun getHouseDetail(url: String): Response<GotResponse> {
        val mock = GotResponse(
            "",
            "House Name",
            "House Region",
            "Coat of arms",
            "",
            listOf(),
            listOf(),
            "",
            "",
            "",
            "",
            "",
            "",
            listOf(),
            listOf(),
            listOf()
        )
        return Response.success(mock)
    }
}