package com.ir2pid.mhp

import com.ir2pid.mhp.data.DetailApiTest
import com.ir2pid.mhp.data.MainApiTest
import com.ir2pid.mhp.ui.detail.DetailRepositoryTest
import com.ir2pid.mhp.ui.detail.DetailViewModelTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses

@RunWith(Suite::class)
@SuiteClasses(
    MainApiTest::class,
    DetailRepositoryTest::class,
    DetailViewModelTest::class,
    DetailApiTest::class
)
class SuiteUnitTest