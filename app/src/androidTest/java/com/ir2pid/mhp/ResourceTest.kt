package com.ir2pid.mhp

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class ResourceTest {
    private lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    fun testAppNameShouldNotChange() {
        assertThat(context.getString(R.string.app_name)).isEqualTo("ir2pid")
    }
}