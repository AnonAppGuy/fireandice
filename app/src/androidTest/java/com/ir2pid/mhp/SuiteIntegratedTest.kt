package com.ir2pid.mhp

import com.ir2pid.mhp.ui.main.MainActivityTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses

@RunWith(Suite::class)
@SuiteClasses(MainActivityTest::class, ResourceTest::class, ExampleInstrumentedTest::class)
class SuiteIntegratedTest